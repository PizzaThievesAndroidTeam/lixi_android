package com.example.hoangyen.testlogin;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;


/**
 * Created by HoangYen on 1/25/2016.
 */
public class choose_gift_activity extends MainActivity {

    public static int[] prgmImages = {R.drawable.avt,
            R.drawable.vdd,
            R.drawable.avt,
            R.drawable.vd,
            R.drawable.avt,
            R.drawable.vdd,
            R.drawable.avt,
            R.drawable.vd,
            R.drawable.avt,
            R.drawable.avt,
            R.drawable.vd,
            R.drawable.avt,
            R.drawable.vd};

    GridView grd;
    TextView txt;
    CallbackManager callbackManager;
    EditText txt_write;
    String loichuc;
    int select_image = -1;
    private ShareButton shareButton;
    //image
    Bitmap image;
    //counter
    int counter = 0;
    View lastTouchedView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.choose_gift_layout);
        txt_write = (EditText) findViewById(R.id.txt_write);
        loichuc = txt_write.getText().toString();

        txt = (TextView) findViewById(R.id.txtContinue);
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select_image == -1) {
                    Toast.makeText(choose_gift_activity.this,
                            "Choose gift please", Toast.LENGTH_LONG).show();
                } else {
                    //postPicture();
                    sharePhotoToFacebook(loichuc, prgmImages[select_image]);
                    Log.d("loichuc", loichuc);
                    Toast.makeText(choose_gift_activity.this,
                            "Shared success", Toast.LENGTH_LONG).show();
                }
            }
        });
        grd = (GridView) findViewById(R.id.grdview);
        grd.setAdapter(new CustomAdapter(this, prgmImages));
        grd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
//                select_image = position;
//                lastTouchedView.setBackgroundColor(Color.WHITE);
//                lastTouchedView = view;
                //view.setBackgroundColor(Color.CYAN);
                //Log.d("aaa", loichuc);
//                String aa = String.valueOf(position);
//                Toast.makeText(choose_gift_activity.this,
//                        aa, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void sharePhotoToFacebook(String caption, int image_select) {
        Bitmap image = BitmapFactory.decodeResource(getResources(), image_select);
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption(caption)
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        ShareApi.share(content, null);

    }
//    public void postPicture(int image_select) {
//        //check counter
//        if(counter == 0) {
//
//            //save the screenshot
////            View rootView = findViewById(android.R.id.content).getRootView();
////            rootView.setDrawingCacheEnabled(true);
//            // creates immutable clone of image
//            image = BitmapFactory.decodeResource(getResources(), image_select);
//            // destroy
//            //rootView.destroyDrawingCache();
//
//            //share dialog
//            AlertDialog.Builder shareDialog = new AlertDialog.Builder(this);
//            shareDialog.setTitle("Share Screen Shot");
//            shareDialog.setMessage("Share image to Facebook?");
//            shareDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    //share the image to Facebook
//                    SharePhoto photo = new SharePhoto.Builder().setBitmap(image).build();
//                    SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
//                    shareButton.setShareContent(content);
//                    counter = 1;
//                    shareButton.performClick();
//                }
//            });
//            shareDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.cancel();
//                }
//            });
//            shareDialog.show();
//        }
//        else {
//            counter = 0;
//            shareButton.setShareContent(null);
//        }
//    }

//    protected void postPicToWall(String userID, String msg, String caption, String picURL) {
//
//        try {
//
//            String response = this.request((userID == null) ? "me" : userID);
//
//            Bundle params = new Bundle();
//            params.putString("message", msg);
//            params.putString("caption", caption);
//            params.putString("picture", picURL);
//
//            response = mFacebook.request(((userID == null) ? "me" : userID) + "/feed", params, "POST");
//
//            Log.d("Tests", response);
//            if (response == null || response.equals("") ||
//                    response.equals("false")) {
//                Log.v("Error", "Blank response");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}

