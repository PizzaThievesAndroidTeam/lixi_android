package com.phamduylinh.tettettet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;

/**
 * Created by stormdev on 1/25/16.
 */
public class SelectGiftActivity extends MainActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_gift);

        GridView gridview = (GridView) findViewById(R.id.grid_select);
        gridview.setAdapter(new ImagesAdapter(this));


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Toast.makeText(SelectGiftActivity.this, "" + position,
                        Toast.LENGTH_SHORT).show();
            }
        });
        TextView textcontinue = (TextView) findViewById(R.id.tv_continue);
        textcontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.img_select);
                SharePhoto photo =  new  SharePhoto . Builder ()
                        . setBitmap ( image )
                        . build();
                SharePhotoContent content =  new  SharePhotoContent . Builder ()
                        . addPhoto ( photo )
                        . build ();
                ShareApi.share(content, null);
            }
        });

    }

}
