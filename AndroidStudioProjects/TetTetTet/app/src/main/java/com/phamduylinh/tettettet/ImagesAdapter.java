package com.phamduylinh.tettettet;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by stormdev on 1/25/16.
 */
public class ImagesAdapter extends BaseAdapter {
    private Context mContext;

    public ImagesAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setOnClickListener ( new  View .OnClickListener ()  {

                @Override
                public  void onClick ( View view )  {
                    Log. d("onClick", "position [" + position + "]");
                }

            });

        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select,
            R.drawable.img_select, R.drawable.img_select

    };
}
