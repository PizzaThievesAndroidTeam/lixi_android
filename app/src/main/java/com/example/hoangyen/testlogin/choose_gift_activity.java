package com.example.hoangyen.testlogin;


import android.app.AlertDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by HoangYen on 1/25/2016.
 */
public class choose_gift_activity extends MainActivity {

    public static int[] prgmImages = {R.drawable.camera_icon,
            R.drawable.vdd,
            R.drawable.avt,
            R.drawable.vd,
            R.drawable.avt,
            R.drawable.vdd,
            R.drawable.avt,
            R.drawable.vd,
            R.drawable.avt,
            R.drawable.avt,
            R.drawable.vd,
            R.drawable.avt,
            R.drawable.vd};
    Context mContext;
    GridView grd;
    TextView txt;
    CallbackManager callbackManager;
    EditText txt_write;
    String loichuc;
    int select_image = -1;
    private ShareButton shareButton;
    Bitmap image;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 1;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.choose_gift_layout);
        txt_write = (EditText) findViewById(R.id.txt_write);
        loichuc = txt_write.getText().toString();
        shareButton = (ShareButton)findViewById(R.id.share_button);
        txt = (TextView) findViewById(R.id.txtContinue);
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select_image == -1) {
                    Toast.makeText(choose_gift_activity.this,
                            "Choose gift please", Toast.LENGTH_LONG).show();
                } else {
                    //postPicture();
                    sharePhotoToFacebook(loichuc, prgmImages[select_image], null);
                    Log.d("loichuc", loichuc);
                    Toast.makeText(choose_gift_activity.this,
                            "Shared success", Toast.LENGTH_LONG).show();
                }
            }
        });
        grd = (GridView) findViewById(R.id.grdview);
        grd.setAdapter(new CustomAdapter(this, prgmImages));
        grd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                select_image = position;
                if (position == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                    sharePhotoToFacebook("Happy new",0,fileUri);
                }
            }
        });
    }
    private void sharePhotoToFacebook(String caption, int image_select, Uri fileUri) {
        if(!Uri.EMPTY.equals(fileUri) && image_select == 0)
        {
            InputStream imageStream = null;
            try {
                imageStream = getContentResolver().openInputStream(
                        fileUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            image = BitmapFactory.decodeStream(imageStream);
        }
        else {
            image = BitmapFactory.decodeResource(getResources(), image_select);
        }
            AlertDialog.Builder shareDialog = new AlertDialog.Builder(this);
            shareDialog.setTitle("Share Image");
            shareDialog.setMessage("Share image to Facebook?");
            shareDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //Log.d("dgg","aaaa");
                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(image)
                            .build();
                    SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
                    shareButton.setShareContent(content);
                    shareButton.performClick();
                }
            });
            shareDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            shareDialog.show();
    }
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }
    private static File getOutputMediaFile(int type){

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        }
        else {
            return null;
        }

        return mediaFile;
    }
}

