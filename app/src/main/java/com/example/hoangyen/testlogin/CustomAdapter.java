package com.example.hoangyen.testlogin;

/**
 * Created by HoangYen on 12/5/2015.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class CustomAdapter extends BaseAdapter {

    Context context;
    int [] imageId;
    private static LayoutInflater inflater=null;
    public CustomAdapter(choose_gift_activity choose_gift,int[] prgmImages) {
        // TODO Auto-generated constructor stub

        context=choose_gift;
        imageId=prgmImages;

        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return imageId.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.item_gridview, parent,false);
        holder.img=(ImageView) rowView.findViewById(R.id.imgDetail);
        holder.img.setImageResource(imageId[position]);
        return rowView;
    }
}
